﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;

namespace webservice_challenge
{
    /// <summary>
    /// Summary description for convert
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class convert : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public double Fibonacci(int n)
        {
            if (n > 100 || n < 1) return -1;
            double a = 0;
            double b = 1;
            // In N steps compute Fibonacci sequence iteratively.
            for (int i = 0; i < n; i++)
            {
                double temp = a;
                a = b;
                b = temp + b;
            }
            System.Threading.Thread.Sleep(50);
            return a;
        }

        [WebMethod]
        public string xmlToJson(String xml)
        {
            string json = "";
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(xml);           
                json = JsonConvert.SerializeXmlNode(doc);
            }
            catch (Exception ex)
            {
                return "Bad Xml format";
            }
            
            return json;       
        }
    }
}
