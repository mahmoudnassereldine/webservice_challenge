﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace consumeWebService
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {

            int param = int.Parse(textBox1.Text);
            Task<String> task = new Task<String>(()=>Fibo(param));            
            task.Start();
            Console.WriteLine("loading");
            pictureBox1.Show();
            String serie = await task;
            Console.WriteLine(serie);
            pictureBox1.Hide();          
            MessageBox.Show(serie);
        }


        public String Fibo (int param)
        {
            if (param > 100 || param < 1) return "-1";

            String result = "";
            for(int i = 1; i<= param; i ++) { 
            var f = new Fibonacci.convertSoapClient();
                result += f.Fibonacci(i) + " ";
            }            
            return result;
        }


        public String xmlToJSON (String xml)
        {           
            var f = new Fibonacci.convertSoapClient();             
            return f.xmlToJson(xml);
        }
        
        private async void button2_Click(object sender, EventArgs e)
        {
            String xml = richTextBox1.Text;
            Task<String> task = new Task<String>(() => xmlToJSON(xml));
            task.Start();
            Console.WriteLine("loading");
            pictureBox1.Show();
            String json = await task;
            Console.WriteLine(json);
            pictureBox1.Hide();
            richTextBox2.Text = json;
        }
                                    

        private void label1_Click(object sender, EventArgs e)
        {
                       
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

      
    }
}
